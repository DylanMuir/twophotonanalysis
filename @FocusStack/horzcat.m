function oStack = horzcat(varargin)

% cat - METHOD Overloaded 'horzcat' method

oStack = cat(nan, varargin{:});

% --- END of horzcat METHOD ---
