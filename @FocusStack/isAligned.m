function bAligned = isAligned(fsStack)

bAligned = ~isempty(fsStack.mfFrameShifts);
